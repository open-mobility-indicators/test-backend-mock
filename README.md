# Backend mock

## Docker stack

Docker stack is defined in `docker-compose.yaml`. To run it:

```bash
docker-compose up
```

Launched services:

- tile server: <http://localhost:8000/services/>
- indicator metadata server: <http://localhost:8001/> (e.g. <http://localhost:8001/catalogs/production.json>)

## Resources

Available in sub-directories of `resources`:

- `catalogs`: mocked production catalog
- `mbtiles`: mocked mbtiles files for tile server
- `raw-indicators`: mocked pipeline outputs

## How to use in carto frontend

To use this mocked backend with the [carto frontend](https://gitlab.com/open-mobility-indicators/carto-v2), set those values in your `.env`:

```env
VITE_CATALOG_URL=http://localhost:8080/files/catalogs/production.json
VITE_RAW_INDICATORS_BASE_URL=http://localhost:8080/files/raw-indicators
VITE_TILE_SERVER_SERVICES_URL=http://localhost:8080/mbtileserver/services
```
