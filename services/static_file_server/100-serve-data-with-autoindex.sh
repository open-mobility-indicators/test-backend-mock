#!/bin/sh

# This script modifies the default config of nginx in-place in order to display a directory index of /usr/share/nginx/html/data
# To do so, it removes the index.html file and enables the autoindex module.

set -e

sed -i -e "s#root   /usr/share/nginx/html;#root   /usr/share/nginx/html/data;#" /etc/nginx/conf.d/default.conf
sed -i -e "s/index  index.html index.htm;/autoindex on;/" /etc/nginx/conf.d/default.conf
